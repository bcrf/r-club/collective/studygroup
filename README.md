# R Club StudyGroup

Repository for R-club "collective" group.  
See also: https://bcrf.biochem.wisc.edu/r-club/

**Where** :The _Biochemistry **R** Club_ sessions are held in room 201, Biochemistry Laboratories, 433 Babcock Drive.  Contact: Jean-Yves Sgro, jsgro@wisc.edu

**Schedule**: We meet every other Tuesday in room 201 at 4pm.

## Other on-going R discussion group:

[Computational Biology, Ecology, & Evolution](https://combee-uw-madison.github.io/studyGroup/)
**ComBEE** is a group of researchers at UW-Madison interested in computational biology in ecology and evolution.

**R Study group meets** every other week, Wednesdays at 10am in room 1153/54 Discovery Building [map](https://maps.google.com/maps?hl=en&q=1153%2F54%20Discovery%20Building). The conference room is on the first floor of the Discovery Building on the west side (windows of the conference room face Randall / The Library Bar Café) in Hub Central.
SEE Monthly ComBEE Events Calendar at bottom of [page](https://combee-uw-madison.github.io/studyGroup/)

See also "**ComBEE** ***Monthly*** **Meeting**" on the same calendar. For example:
- Topic: RNA seq / transcriptomics
- When: Tue, October 31, 4:15pm – 5:15pm
- Where: Microbial Sciences Building (MSB) 1550 Linden Drive, room 6201 [map](https://maps.app.goo.gl/UsfkkhewNBERFJMg9)

***Other Campus Classes**: I made a list of free instruction at [R and Python workshops on Campus – Fall 2023](https://bcrf.biochem.wisc.edu/2023/08/23/r-and-python-workshops-on-campus-fall-2023/)

## Fall 2023

We are starting this session with [**R for Data Science** (2e)](https://r4ds.hadley.nz/) since the first author, Hadley Wickham, is the principal author of the modernization of R with the "Tidyverse" series packages.

## Spring 2023
For this first incarnation there will only be one group since most registrants seem more on the beginner side.

We’ll meet every other Tuesday in room 201 (alternating with a Python Club group.)  
The first session will be February 07, 2023.

Materials

We will (loosely) follow the following book, freely available as online version: http://rafalab.dfci.harvard.edu/dsbook/

***Introduction to Data Science*** – **Data Analysis and Prediction Algorithms with R**
by Rafael A. Irizarry,  Professor of Applied Statistics at Harvard and the Dana-Farber Cancer Institute,Chair of the Department of Data Sciences at the Dana-Farber Cancer Institute and Professor of Biostatistics at Harvard T.H. Chan School of Public Health.

Book source available at https://github.com/rafalab/dsbook

**Software**

We will use to learn the programming langage R by using the R software through RStudio software.
If you use your own computer during session you will need to install both (free) software:

– R software installer can be downloaded from “The Comprehensive R Archive Network” ([CRAN](https://cran.r-project.org/))
– The RStudio integrated development environment (IDE): download [RStudio Desktop](https://posit.co/downloads/)


## Code of Conduct

In order to foster a positive and professional learning environment we encourage the following kinds of behaviors in all platforms and events:

– Use welcoming and inclusive language
– Be respectful of different viewpoints and experiences
– Gracefully accept constructive criticism
– Focus on what is best for the community
– Show courtesy and respect towards other community members

(Excerpt from [Carpentries Code of Conduct](https://docs.carpentries.org/topic_folders/policies/code-of-conduct.html#code-of-conduct-summary-view))
