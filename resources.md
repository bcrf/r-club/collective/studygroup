# Useful links

Resources will be added as they are discovered over time...

## RStudio - Posit

RStudio cloud...

Monthly event (Last Tuesday of the month).  Helpful for anyone working with R and Python data projects looking for a centralized, efficient space to manage them.
No registration is required to attend – simply add it to your calendar using https://www.addevent.com/event/Nn18171246

## Colors

- Colors cheat sheet: https://www.nceas.ucsb.edu/sites/default/files/2020-04/colorPaletteCheatsheet.pdf

## Statistics

- testing for normality and what each test means: https://cran.r-project.org/doc/contrib/Shipunov-visual_statistics.pdf

## Tidyverse

NEW: **what’s new in the tidyverse in 2023?**  

🔗 mine.quarto.pub/tidyverse-2023    
💻 github.com/mine-cetinkaya-rundel/tidyverse-2023   
Author: dr. mine çetinkaya-rundel  duke university + posit  2023-05-30   

---

*The tidyverse is an opinionated collection of R packages designed for data science. All packages share an underlying design philosophy, grammar, and data structures.*

The main site is [www.tidyverse.org/](https://www.tidyverse.org/) with useful subdivisions:  
- [Learn](https://www.tidyverse.org/learn/): Books (free online), workshops...  
- [Packages](https://www.tidyverse.org/packages/). 

-[Blog](https://www.tidyverse.org/blog/)  
- - [Differences between the base R and magrittr pipes](https://www.tidyverse.org/blog/2023/04/base-vs-magrittr-pipe/)

There are many more resources online:

- [Tidyverse Skills for Data Science](https://jhudatascience.org/tidyversecourse/) - Carrie Wright, Shannon E. Ellis, Stephanie C. Hicks and Roger D. Peng - 2021-09-02

- [The tidyverse style guide](https://style.tidyverse.org)

- [Introduction to R - tidyverse](https://bookdown.org/ansellbr/WEHI_tidyR_course_book/) Brendan R. E. Ansell @ansellbr3

- [An Introduction to R through the tidyverse](https://pmacdasci.github.io/r-intro-tidyverse/) - ([Code on Github](https://github.com/PMacDaSci/r-intro-tidyverse))

- [Data Manipulation and Visualisation using R](http://bioinformatics-core-shared-training.github.io/r-intermediate/) : [Plotting data with ggplot2](https://rawgit.com/bioinformatics-core-shared-training/r-intermediate/master/ggplot2-live-coding-script.html), [**tidying and transforming data**](https://rawgit.com/bioinformatics-core-shared-training/r-intermediate/master/2.dplyr-intro-live-coding-script.html)... and more...  ([code on Github](https://github.com/bioinformatics-core-shared-training/r-intermediate).)

- Cambridge version:  An Introduction to Solving Biological Problems with R
- - Data Manipulation and Visualization in R:
- - [slides](https://rawgit.com/bioinformatics-core-shared-training/r-intermediate/master/1.introduction.html#1) - [Archived](http://web.archive.org/web/20230530234507/https://rawgit.com/bioinformatics-core-shared-training/r-intermediate/master/1.introduction.html)
- Course: http://cambiotraining.github.io/r-intro/ (Code on [Github](https://github.com/cambiotraining/r-intro).)

## Quarto

- [Getting Started with Quarto](https://rstudio-conf-2022.github.io/get-started-quarto/) - by Tom Mock - rstudio::conf 2022 Workshop
- [Quarto](https://r4ds.hadley.nz/quarto.html) in [R for Data Science (2e)](https://r4ds.hadley.nz/)
- [Quarto/RMarkdown - What’s Different?](https://laderast.github.io/qmd_rmd/#/title-slide)

## General Topics

### Transcriptomics

- [refine.bio](https://www.refine.bio/about) is a collection of ready-to-use transcriptomic data. 

## Specific Topics

- [Survival Analysis Using R](https://pmacdasci.github.io/survival-analysis-intro/SurvivalAnalysis_I.html)
- - [Github code](https://github.com/rstudio-conf-2022/get-started-quarto)

## Introductions to R

- [A \{fun\} Intro to R Programming](https://favstats.github.io/ds3_r_intro/#1)
- [Getting Used to R, RStudio, and R Markdown](https://ismayc.github.io/rbasics-book/index.html) - Chester Ismay and Patrick C. Kennedy - 2021-12-30


## 
