## Tidytranscriptomics

Motivation: this session series was motivated by requests from R Club participants to use "genomics" data during sessions. I wanted to add information about the newer way to usr R using "tidytverse" and the "tidytranscriptomics" packages offered both together.

The goal was first to explore and reproduce the material in at least one of these online workshop materials:

* https://stemangiola.github.io/bioc2021_tidytranscriptomics/articles/tidytranscriptomics.html
* https://stemangiola.github.io/ABACBS2020_tidytranscriptomics/articles/tidytranscriptomics.html

The author has github repositories for the "tidytranscriptomics" packages 

* https://github.com/stemangiola/tidybulk
* https://github.com/stemangiola/tidySingleCellExperiment

While the author has published Docker images, they are "amd64" and they do not work for the newer Apple Macintosh with the M1, M1 Max, M2 etc. CPU "arm64" architechture. Therefore I embarked on recreating Docker images suitable for these chips. My Docker images are located at: 

* home page: https://hub.docker.com/r/jysgro/tidytranstriptomics
* tagged versions: https://hub.docker.com/r/jysgro/tidytranstriptomics/tags

See below for launching command

I made "amd64" as well as "arm64" versions starting with various Bioconductor Docker sources.

## In Class

In class we used the [bioc2021](https://stemangiola.github.io/bioc2021_tidytranscriptomics/articles/tidytranscriptomics.html) version of the online workshop.

In order to avoid a lengthy installation, in class we used 2 different Docker images, one suited for Windows ("amd64" architechture) and one suited for the new M1 Class Mac ("arm64"). For this users need to have Docker installed (see [`www.docker.com`](https://www.docker.com/).)

The images can be donwloaded ("pulled") with the following command:

* for Windows/Linux with Intel ("amd64") CPU:

```
docker pull jysgro/tidytranstriptomics:3.17
```

(`jysgro/tidytranstriptomics:3.16` is another "amd64" option.)

* For M1 ("arm64") CPU:

```
docker pull jysgro/tidytranstriptomics:arm64_3.16-R-4.2.2
```

A Docker image is the "run" to create a  temporary "container" which, in this case, will run the Linux (Ubuntu) operating system and deploy a web service to access R and RStudio. The command to run would be:

* for Windows/Linux with Intel ("amd64") CPU, given at a PowerShell or cmd console:

```
docker run --rm -e PASSWORD=abc -p 8787:8787  jysgro/tidytranstriptomics:3.17
```

* For M1 ("arm64") CPU:

```
docker run --rm -e PASSWORD=abc -p 8787:8787  jysgro/tidytranstriptomics:arm64_3.16-R-4.2.2 
```


ONCE the docker container is launched, you can access RStudio within a web browser:

* Got to: [`localhost:8787/`](localhost:8787/)
* username: `rstudio`
* password: `abc` or what you chose for the *mandatory* `PASSWORD` variable on the *run* line.

*Note*: the `--rm` option will delete the "container" once the job is done. For these demonstration it is not necessary to share a directory. However, should the user want to access data on the hard drive, additional options need to be added on the `docker` command line. For example adding **`-v ${PWD}:/home/rstudio`** will share the content of the **current directory** and files can also be saved there. This notation is suitable for all platform.

When you are done, you can quit the browser. However, you need to signal the Docker container that your job is finished. On the Terminal that you used to launch, isse a "Control" "C" command. It may take a few seconds, but the job will stop. Thanks to the added `--rm` command the container will be discarded (but the image remains on your computer, ready to be used again to create a brand new container.)

## Code modification

We followed the [bioc2021](https://stemangiola.github.io/bioc2021_tidytranscriptomics/articles/tidytranscriptomics.html) version of the online workshop.  The code is visible in a different form on [github](https://github.com/stemangiola/bioc2021_tidytranscriptomics). We limited the demonstration to that of "bulk RNA-seq."

However, there is one change that needs to be done on some of the code, namely changing `sample` for `.sample` and replacing `feature ` by `.feature`.

For example the following in the code:

```
airway %>%
    group_by(sample) %>%
    summarise(n=n())
```

we need to replace `sample` by `.sample`:

```
airway %>%
    group_by(.sample) %>%
    summarise(n=n())
```

In this code the variable `sample` neeed to be changed twice to `.sample`:

```
airway %>%
  mutate(sample_name=str_remove(.sample, "SRR1039")) %>%
  # select columns to view    
  select(.sample, sample_name)
```

Each command with such words need to be updated.

In this code the word `feature` was updaded to `.feature`:

```
de_all %>%
  pivot_transcript() %>%
  select(edgerQLT_PValue, edgerLR_PValue, voom_P.Value, deseq2_pvalue, .feature) %>%
  ggpairs(1:4)
```

In class we stopped after the volcano plots.

If there is further insterest we can explore the Single Cell options in a subsequent R Club session.

## Tidy Code

One of the goals of this session was to explore the new, "tidy" R code. As such, the `tidybulk` github page shows in detail how code is written the "tidy" way and compare to the "Standard procedure." The use of the `%>%` pipes makes the code more streamlined, shorter, and requires less intermediate variables or objects.

See: https://github.com/stemangiola/tidybulk

