## Docker images

I based the Docker images on the [bioconductor/bioconductor_docker](https://hub.docker.com/r/bioconductor/bioconductor_docker) series. 

I added some `#` remarks in the Dockerfile.

First attempts using the `stemangiola` [Dockerfile](https://github.com/stemangiola/bioc2021_tidytranscriptomics/blob/master/Dockerfile) failed regardless of the many attempts, usually with major dependencies errors.



Fortunately I discovered a LinkedIn post with this PDF: [Cheat sheet: fast container builds for R](https://raw.githubusercontent.com/sib-swiss/containers-introduction-training/main/r-docker-cheatsheet/r-docker-cheatsheet.pdf)

which is better explained in this Stack Overflow post:
[Install R packages using docker file](https://stackoverflow.com/questions/45289764/install-r-packages-using-docker-file) using the `install2.R` functionality of the `littler` package.

* Demonstration: https://eddelbuettel.github.io/r2u/

Switching to `install2.r` for R packages and `/usr/local/lib/R/site-library/littler/examples/installBioc.r` for Bioconductor packages made the difference: no more ERROR: dependencies...
