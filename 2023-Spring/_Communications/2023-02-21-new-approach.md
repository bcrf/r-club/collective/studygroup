## R Club  - 21 Feb 2023

The studied material is now uploaded on the repository in [ 2023-02-21-session-02](https://git.doit.wisc.edu/bcrf/r-club/collective/studygroup/-/tree/main/2023-Spring/2023-02-21-session-02)

To emphasize how *RMarkdown* can output "finished documents" in multiple formats I have converted both `*.Rmd` files to all readily available format: `html`, `pdf`, and `docx` which is done by using the `knitr` button in RStudio.

## New book, alternate approch

While discussing material during the session, there seemed to be a consensus to approach learning R differently: "finding existing code and leaning what it does by decomposing its structure, commands and logic."

Many of those in attendance are interested in genomics data. For the next session (March 7^th ) we'll test this method by choosing a chapter in a new book: ***"Molecular Data Analysis Using R"*** (2017, Csaba Ortutay and Zsuzsanna Ortutay.)

> ***The book is available online*** [^1] *via* the UW Library [^2]. 
> `Code` from the book is also available online [^3], [^4]

***You can suggest which chapter*** you would like to test first, based on content and subject.

It does not mean that the other book is not longer useful. We'll have to see what is best in learning at the R Club.

## How I learned R

My encounter with R dates back to 2005 when I started helping with microarray data analysis. But I first had to learn enough R... and there were no workshops or club at that time.

Then I found a very helpful little book to let me learn basic R by following the exercises. Interestingly it is available in [English](https://cran.r-project.org/doc/contrib/Paradis-rdebuts_en.pdf), [French](https://cran.r-project.org/doc/contrib/Paradis-rdebuts_fr.pdf), [Spanish](https://cran.r-project.org/doc/contrib/rdebuts_es.pdf), and [Romanian](https://cran.r-project.org/doc/contrib/Paradis-rdebuts_RO.pdf). 
The contents of the book is still very relevant to learn *"base R"* which is the default installation. At my very beginning this little booklet helped me a lot, and yes... I even used the French version!

Nowadays there are augmented packages such as `tidyverse` and `ggplot` that can help write better R code. However, the knowledge of *"base R"* is very helpful in every day data analysis, online searches often yield solutions in *"base R"*.

### R Club links

As a reminder here are the R CLub links:

- R CLub page: https://bcrf.biochem.wisc.edu/  
- R Club repository: https://git.doit.wisc.edu/bcrf/r-club/collective/studygroup

---

[^1]: The library then refers to this link at: [ebookcentral.proquest.com](https://ebookcentral.proquest.com/lib/wisc/detail.action?pq-origsite=primo&docID=4774521). Howewver, it may be necessary to start the search at library.wisc.edu .

[^2]: UW Library catalog number: https://search.library.wisc.edu/catalog/9912822968302121/online

[^3]: Companion site, www.wiley.com/go/ortutay/molecular_data_analysis_r

[^4]: GitHub: https://github.com/csortu/MDAuR
