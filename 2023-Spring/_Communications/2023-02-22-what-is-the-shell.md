## R Club - What is the shell?

*Note* This is message ***not about R***...!

During the session I mentioned "the shell" and that raised some questioning eyes... ***"What is the shell?"***

The shell is a text-based command language developed in the early days of computing so that *humans* could communicate with the computer hardware. The commands are usually derived from English words or short phrases, and were short enough to not use too much space on these old systems. For example, the command to "**c**hange **d**irectory" is `cd` and to "make a new directory" it is `mkdir`. Listing files is `ls`. The most common shell is `bash` (Bourne Again Shell) which is a re-write of the older shell `sh` written by [Steven Bourne](https://en.wikipedia.org/wiki/Stephen_R._Bourne).

The Unix shell contains a lot of small utilities that can be stringed together (the fancy word is '*piping*', as the data '*flows*' ... the output of one utility serving as the input for the next.

In fact I very recently wrote a short blog entry showing how to combine a few Unix utilities in a way to compute the composition of amino acid or nucleotide sequences very easily, and without programming.

The article is here: [**Fast Sequence Composition**](https://bcrf.biochem.wisc.edu/2023/02/21/fast-sequence-composition/)

### Macintosh is Unix

Since about the year 2000, a Macintosh is a Unix system at its core (inherited from Steve Jobs **NeXt** company.) As such there is a text `Terminal` with a shell. The new default shell `zsh` works almost the same as `bash`.

### What about Windows?
Windows systems also started with a plain command-line system called [DOS](https://en.wikipedia.org/wiki/DOS) (Disk Operating System) a very long time before the first "Windows GUI." 
In more modern Windows there is now [PowerShell](https://en.wikipedia.org/wiki/PowerShell) to help accomplish complex task.

However, neither DOS nor PowerShell are well known to the general public and do not conform to the Unix nomenclature.

Microsoft understood this a few years ago when it created the possibility to install Ubuntu Linux under the hood in Windows. To learn more about this see [Install Linux on Windows with WSL](https://learn.microsoft.com/en-us/windows/wsl/install) ([Archived - 2023-02-21](https://web.archive.org/web/20230221150744/https://learn.microsoft.com/en-us/windows/wsl/install).)
