# Session 5 foreword

During previous sessions there was an interest to explore genomic data. The book below has a good introduction to both R and genomic data analysis.

This document is a summary of what was done during the session. Most of the explanatory details should be consulted in this proposed book.

This document is written in basic R markdown (`*.Rmd`) and compiled in multiple formats. 

## Exploring Genomics Data with R

Book: 
[Computational Genomics with R, by Altuna Akalin, 2020-09-30](https://compgenomr.github.io/book/)[^1]

[^1]:https://compgenomr.github.io/book/

The book source code is available on github[^2] 

[^2]:https://github.com/compgenomr/book

The data from the book is also available[^3]

[^3]:https://github.com/compgenomr/compGenomRData

---
Footnotes / Links

