---
title: "Session5 - Genomics Data"
author: "Jean-Yves Sgro"
date: "2023-03-21"
output:
  html_document: default
  word_document: default
  pdf_document: default
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
```

# Session 5 foreword

During previous sessions there was an interest to explore genomic data. The book below has a good introduction to both R and genomic data analysis.

This document is a summary of what was done during the session. Most of the explanatory details should be consulted in this proposed book.

This document is written in basic R markdown (`*.Rmd`) and compiled in multiple formats. 

## Exploring Genomics Data with R

Book: 
[Computational Genomics with R, by Altuna Akalin, 2020-09-30](https://compgenomr.github.io/book/)[^1]

[^1]:https://compgenomr.github.io/book/

The book source code is available on github[^2] 

[^2]:https://github.com/compgenomr/book

The data from the book is also available[^3]

[^3]:https://github.com/compgenomr/compGenomRData

# RNA-Seq

We jump directly into RNA-Seq analysis information in [Chapter 8](https://compgenomr.github.io/book/rnaseqanalysis.html) [^4]

[^4]:https://compgenomr.github.io/book/rnaseqanalysis.html

## Count table

The data provided will be in the form of a *count table* which lists genes in rows and samples in columns, showing how many reads were mapped to a given gene for each sample, represented by `x` in the table below.

| genes  | sample1  |  sample1 |  case1 | case2  |
|---|---|---|---|---|
| gene 1   |  x |  x |  x | x  |
|  gene 2 |  x |  x | x  | x  |
|  ... |  x | x  |  x |  x |
|  gene Z |  x |  x |  x |  x |

# Get data installed

The data can be installed with the following command, but it often fails.

```{r eval=FALSE}
options(timeout=300)
devtools::install_github("compgenomr/compGenomRData")
```

The author suggests to download from https://github.com/compgenomr/compGenomRData/releases/tag/v1.0 and use the following command in a Terminal:

```bash
R CMD INSTALL compGenomRData-1.0.tar.gz
```


## Read files

The data used is from the `compGenomRData` package, located within an directory called `extdata`. We first prepare 2 variables to contain the location of the files. The third command actually reads-in the *count table* into object `counts`.

```{r}
#colorectal cancer
counts_file <- system.file("extdata/rna-seq/SRP029880.raw_counts.tsv",
                           package = "compGenomRData")
coldata_file <- system.file("extdata/rna-seq/SRP029880.colData.tsv",
                            package = "compGenomRData")

counts <- as.matrix(read.table(counts_file, header = T, sep = '\t'))
```

We can use the command `View(counts)` to open the `counts` object into a visual table. The same thing can be accomplished by clicking on `counts` within the `Environment` tab in RStudio.

## Normalizations

Various versions of normalization are computed.

The `width` column of the data contains the length of genes.

### CPM

Column `width` is removed from the computation.

```{r}
cpm <- apply(subset(counts, select = c(-width)), 2, 
             function(x) x/sum(as.numeric(x)) * 10^6)
```

Check that the sum of each column after normalization equals to 10^6 (except the width column).

```{r}
colSums(cpm)
```

### RPKM

```{r}
# create a vector of gene lengths 
geneLengths <- as.vector(subset(counts, select = c(width)))

# compute rpkm 
rpkm <- apply(X = subset(counts, select = c(-width)),
              MARGIN = 2, 
              FUN = function(x) {
                10^9 * x / geneLengths / sum(as.numeric(x))
               })
```

Check the sample sizes of RPKM. Notice that the sums of samples are all different.

```{r}
colSums(rpkm)
```

### Computing TPM

```{r}
#find gene length normalized values 
rpk <- apply( subset(counts, select = c(-width)), 2, 
              function(x) x/(geneLengths/1000))
#normalize by the sample size using rpk values
tpm <- apply(rpk, 2, function(x) x / sum(as.numeric(x)) * 10^6)
```

Check the sample sizes of `tpm`. Notice that the sums of samples are all equal to 10^6.

```{r}
colSums(tpm)
```

## Exploratory analysis of the read count table

### Clustering

We first select the best 100 genes.

```{r}
#compute the variance of each gene across samples
V <- apply(tpm, 1, var)
#sort the results by variance in decreasing order 
#and select the top 100 genes 
selectedGenes <- names(V[order(V, decreasing = T)][1:100])
```

Now we can quickly produce a heatmap where samples and genes are clustered.

We can reduce the plot to only the 100 `selectedGenes` to subset `tpm` with `tpm[selectedGenes,]`.

```{r}
library(pheatmap)
pheatmap(tpm[selectedGenes,], scale = 'row', show_rownames = FALSE)
```

Adding annotations:

Note that `stringsAsFactors = TRUE` is (again) necessary starting with R version 4.x.

```{r}
colData <- read.table(coldata_file, header = T, sep = '\t', 
                      stringsAsFactors = TRUE)
pheatmap(tpm[selectedGenes,], scale = 'row', 
         show_rownames = FALSE, 
         annotation_col = colData)
```

### PCA

Principle component analysis.

```{r}
library(stats)
library(ggplot2)
library(ggfortify)
#transpose the matrix 
M <- t(tpm[selectedGenes,])
# transform the counts to log2 scale 
M <- log2(M + 1)
#compute PCA 
pcaResults <- prcomp(M)

#plot PCA results making use of ggplot2's autoplot function
#ggfortify is needed to let ggplot2 know about PCA data structure. 
autoplot(pcaResults, data = colData, colour = 'group')
```


We can also change the size of the points if they are too small, and we can also change the theme:

```{r}
autoplot(pcaResults, data = colData, colour = 'group', size = 5) +
  theme_bw()
```

A summary computation can show relative size of components:

```{r}
summary(pcaResults)
```

## Correlation plots

pairwise correlation scores:

```{r}
library(stats)
correlationMatrix <- cor(tpm)
```

First plot:

Note: `number.cex = 0.7` defines the size of the number within the bubbles.

```{r}
library(corrplot)
corrplot(correlationMatrix, order = 'hclust', 
         addrect = 2, addCoef.col = 'white', 
         number.cex = 0.7) 
```

heatmap version

```{r}
library(pheatmap)
# split the clusters into two based on the clustering similarity 
pheatmap(correlationMatrix,  
         annotation_col = colData, 
         cutree_cols = 2)
```

