## Slides

These slides were generated automatically by script. See [`chapter_to_slides.R`](https://github.com/rafalab/dsbook/blob/master/chapter_to_slides.R) in original book site  on github: [dsbook](https://github.com/rafalab/dsbook).

