## Session 02 - February 21, 2023

We will review content of Chapter 2 [R Basics](http://rafalab.dfci.harvard.edu/dsbook/r-basics.html) with ["The very basics"](http://rafalab.dfci.harvard.edu/dsbook/r-basics.html#the-very-basics) subsection, and first set of exercises [section 2.3](http://rafalab.dfci.harvard.edu/dsbook/r-basics.html#exercises)

I have added a new "Project" (`Session-02.Rproj`file) and a start-up R Markdown (`R-Basics.Rmd`) file which was converted to final document formats (`pdf`, `html`, and `docx`) with `knitter` as we learned in Session-01.

I added the `inclass.Rmd` file and all its rendered versions. This was created during the session, and I added more comments afterwards. 

*Note*: when a code chunk contains more than one command, all results are combined and printed together. The symbols `## ` preceed all output by default. This effect were obtained by adding `results="hold"`.

I added directory `slides` that contains material from Chapter 2. See `readme.md` within.
