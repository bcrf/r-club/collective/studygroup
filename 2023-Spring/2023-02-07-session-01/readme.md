# First R Club session

- Session **summary**: see below
- optional homework: [Chapter 2 R basics](http://rafalab.dfci.harvard.edu/dsbook/r-basics.html)

## Summary

We met for a 2h session in 201. We went over the following material and concepts

- R and RStudio interface: [Chapter 1 Getting started with R and RStudio](http://rafalab.dfci.harvard.edu/dsbook/getting-started.html)
- we followed the instructions in the chapter to NOT save a file called `.RData` when we quite RStudio. The file would preload some data or prefrerences and might interfere with a new computation. This is related to the concept of "reproducible research." 

We also learned about:

- when R is installed it is called **R base**. **Packages** can be added to augment the functionality of the software.
- **R base** contains some built-in datasets that can be readily used. The list of all datasets can be obtained by the command `data()` typed within the R console. We used the dataset `chickwts` (see below.)
- creating a new *Project* with RStudio to contain all of the files within a specific directory. A project file in the form `*.Rproj` can be simply double-clicked to reopen RStudio within that directory.

We also also looked at a data analysis based on this tutorial [Demo yeast mutant analysis](https://static-bcrf.biochem.wisc.edu/tutorials/r/r_extreme_beginner/demo_yeast.html)

### Markdown and RMarkdown

We made a "detour" from R to learn about Markdown and then how to use this within RStudio to create research reports from data that include both the "story" as well as the computation and results, in numbers and/or plots, embedded within the final document.

A markdown document is obtained from the RStudio menu "File > New File... > R Markdown..." which contains a basic example. This document can then be automatically converted to HTML or MSWord with the button called **Knit** that will *weave* together the text, the R code and the plots. the very top of the document contains a header in the YAML format that contains the document title etc. The `output` list is automatically updated.  WIthin the body of rthe document, the R code is contained within curly brackets after 3 backticks and closed by 3 backticks.

````
```{r}
# The R code is added within these backticks.
```
````

In-line code can be added within the text to compute with R. For example:

> In fact the dataset `chickwts` has: **`r dim(chickwts)[2]`** columns and **`r dim(chickwts)[1]`** rows.

would read in the final document:

> In fact the dataset `chickwts` has: **2** columns and **71** rows.


Examples created during the session are included i this directory. R markdown files are `*.Rmd`. The HTML, PDF or MSWord output can be recreated from within RStudio.

*Note*: PDF requires installation of extra software, possibly with the follwing commands given to the R console.(See *e.g.* [pdflatex not found outside of RStudio](https://github.com/wjakethompson/jayhawkdown/issues/1)

```
install.packages("tinytex")
tinytex::install_tinytex()
```

### Note
The `*.Rmd` files are not rendered completely within GitLab. The file should be downloaded rather than use Copy/Paste.



