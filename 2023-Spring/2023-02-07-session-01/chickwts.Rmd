---
title: "chickwts"
output:
  html_document: default
  pdf_document: default
date: "2023-02-07"
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE, comment=NA)
```

# dataset chickwts

The dataset has about `70` lines... But R knows better and it says that there are `r length(chickwts$weight)`

In fact the dataset `chickwts` has: **`r dim(chickwts)[2]`** columns and **`r dim(chickwts)[1]`** rows.

The dataset top lines are:

```{r}
head(chickwts)
```

The bottomlines are:

```{r}
tail(chickwts)
```

We can ask R to make a basic plot directly with its built-in `plot()` function:

```{r}
plot(chickwts)
```

However this plot is not very informative. It would be better to invert the order of the columns before R makes the plot. We can access each column by adding the name of the column to the dataset name separated by a `$` symbol:


```{r}
plot(chickwts$feed, chickwts$weight)
```

This plot is much more informative. Note also that R automatically created *boxplots*.

Another way to write this code is to use the function `with()` that avoids using the `$` notation:

```{r}
with(chickwts, plot(feed, weight))
```

