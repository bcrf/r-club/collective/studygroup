# Today: getting started with R/RStudio for genomics

Summary of preparatory instructions for today: 

1. Open R/RStudio for genomics ["all in one page"](https://datacarpentry.org/genomics-r-intro/aio.html) (Short URL: https://go.wisc.edu/d8gcb7)
2. Create a a new RStudion project in a new directory on your Desktop named *e.g.* `RGenomics` 
3. Download files from the local [./data](data) directory into `RGenomics` genomics directory

Files can be downloaded manually or with the following command lines on a Terminal with either `wget` or `curl`:

### using `curl`

`curl` should be installed by default on all systems.

```
curl -o Ecoli_metadata.xlsx https://git.doit.wisc.edu/bcrf/r-club/collective/studygroup/-/raw/main/2023-Spring/2023-08-08-session-13/data/Ecoli_metadata.xlsx 

curl -o combined_tidy_vcf.csv https://git.doit.wisc.edu/bcrf/r-club/collective/studygroup/-/raw/main/2023-Spring/2023-08-08-session-13/data/combined_tidy_vcf.csv

# vcf_clean_script.R may not be needed after all
curl -o vcf_clean_script.R https://git.doit.wisc.edu/bcrf/r-club/collective/studygroup/-/raw/main/2023-Spring/2023-08-08-session-13/data/vcf_clean_script.R
```

### using `wget`

`wget` is not installed by default: in this case use `curl` above.

```
wget https://git.doit.wisc.edu/bcrf/r-club/collective/studygroup/-/raw/main/2023-Spring/2023-08-08-session-13/data/Ecoli_metadata.xlsx

wget https://git.doit.wisc.edu/bcrf/r-club/collective/studygroup/-/raw/main/2023-Spring/2023-08-08-session-13/data/combined_tidy_vcf.csv

# vcf_clean_script.R may not be needed after all
wget https://git.doit.wisc.edu/bcrf/r-club/collective/studygroup/-/raw/main/2023-Spring/2023-08-08-session-13/data/vcf_clean_script.R
```

## Back to RStudio

On the R/RStudio for genomics ["all in one page"](https://datacarpentry.org/genomics-r-intro/aio.html) use the "Search" function (e.g. Ctrl-F) to locate the first instance of the word `combined_tidy_vcf.csv`.

This will occur a little less than 1/2-way down the long page and will be a paragraph starting with:

> Now, let’s read in the file combined_tidy_vcf.csv 

This will be our starting point for today.

