# Session 13 - R/RStudio for genomics

Summary of email sent to Club members.

August -7, 2023

Next week is a [Data Carpentry](https://uw-madison-datascience.github.io/2023-08-14-uwmadison-dc/) series, part of it is titled R/RStudio for genomics. The series is "sold out" but the material is available online. If there is interest we could do the R/RStudio section:

| Date  | Title    |
|------------|-------------------------|
| August 21  | [Intro to R and dplyr](https://datacarpentry.org/genomics-r-intro/)    |
| August 22  | [R visualization and Rmd](https://datacarpentry.org/genomics-r-intro/) |

Material can also be viewed as a ["all in one page"](https://datacarpentry.org/genomics-r-intro/aio.html), ([Archived 2023-08-08](http://web.archive.org/web/20230808163632/https://datacarpentry.org/genomics-r-intro/aio.html).)

We'll skip the Intro and Basics as we have already explored this in a few of the previous Club sessions.

The actual Data Carpentry class will be taught via AWS, but the [data is available for download](https://figshare.com/articles/dataset/Data_Carpentry_Genomics_beta_2_0/7726454) which can be downloaded as a single archive of about 3.1 Gb for all the sessions. 

However, the R sessions only require the following files. A local copy is also saved in the [`data`](./data) directory.

- [`combined_tidy_vcf.csv`](https://figshare.com/ndownloader/files/14632895): output of `vcf_clean_script.R` ([Archived 2023-08-08](http://web.archive.org/web/20230808164251/https://s3-eu-west-1.amazonaws.com/pfigshare-u-files/14632895/combined_tidy_vcf.csv?X-Amz-Algorithm=AWS4-HMAC-SHA256&X-Amz-Credential=AKIAIYCQYOYV5JSSROOA/20230808/eu-west-1/s3/aws4_request&X-Amz-Date=20230808T164249Z&X-Amz-Expires=10&X-Amz-SignedHeaders=host&X-Amz-Signature=df9807955c03dd37d373b97dc6e4296b7e1930108070ecf706071663e54b7574).)


- [`Ecoli_metadata.xlsx`](https://figshare.com/ndownloader/files/39515053): an example Excel file to be loaded during the R lesson. ([Archived 2023-08-08](https://s3-eu-west-1.amazonaws.com/pfigshare-u-files/39515053/Ecoli_metadata.xlsx?X-Amz-Algorithm=AWS4-HMAC-SHA256&X-Amz-Credential=AKIAIYCQYOYV5JSSROOA/20230808/eu-west-1/s3/aws4_request&X-Amz-Date=20230808T164902Z&X-Amz-Expires=10&X-Amz-SignedHeaders=host&X-Amz-Signature=0f1847c7ca9ecddd319edec6e6a082519aa3ce9a3134dadb0d595db6c0253568).)

This one will probably not be necessary:

- [`vcf_clean_script.R`](https://figshare.com/ndownloader/files/14632898): converts vcf output in the "wrangling" lesson section to single tidy data frame.
