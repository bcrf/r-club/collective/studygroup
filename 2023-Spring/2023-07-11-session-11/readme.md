# Session 11

Attendance: 3

## Discussion

We discussed importing GEO data into R.

One way is to use package [GEOquery](https://bioconductor.org/packages/release/bioc/html/GEOquery.html)

We discussed installing other packages from Bioconductor

There is an online web site for analysing GEO datasets but it is rather clunky and confusing:

[GEO2R](https://www.ncbi.nlm.nih.gov/geo/geo2r/)
