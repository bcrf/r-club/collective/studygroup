# For the record I add the command used in the session.

The code is below and also in the "standard" `.R` format within this directory. 

```
# Session 09
# May 30, 2023

# From:
# Main page: https://rawgit.com/bioinformatics-core-shared-training/r-intermediate/master/2.dplyr-intro-live-coding-script.html
# Code and Data: https://github.com/bioinformatics-core-shared-training/r-intermediate

# Load library

library(tidyverse)
clinical_data <- read_tsv("clinical-data.txt")

clinical_data <- gather(clinical_data, key = "Treatment", value = "Value", -Subject)

ggplot(clinical_data, mapping = aes(x = Treatment, y = Value)) +
  geom_boxplot()

## Separate replicates

clinical_replicate_data <- separate(clinical_data, Treatment, into = c("Treatment", "Replicate"))

ggplot(clinical_replicate_data, mapping = aes(x = Replicate, y = Value)) +
  geom_boxplot() +
  facet_wrap(~Treatment)

# Spread: go back to wide form
spread(clinical_data, key = Treatment, value = Value)

clinical_wide <-spread(clinical_data, key = Treatment, value = Value)

# Selecting columns

patients <- read_tsv("patient-data.txt")
colnames(patients)

select(patients, Name, Sex)

# Transforming and cleaning the data

patients_TSV <- read_tsv("patient-data.txt")
patients <- read.delim("patient-data.txt")
patients

patients <- as_tibble(patients)
patients

ggplot(patients, mapping = aes(x = Sex)) +
  geom_bar()

levels(as.factor(patients$Sex))

levels(as.factor(str_trim(patients$Sex)))

patients$Sex <- str_trim(patients$Sex)

# Tidyverse way:

mutate(patients, Sex = str_trim(Sex))

# Or create a new column:

mutate(patients, SexTrimmed = str_trim(Sex))

# make it a factor:

patients <- mutate(patients, Sex = factor(str_trim(Sex)))
levels(patients$Sex)

ggplot(patients, mapping = aes(x = Sex)) +
  geom_bar()

ggplot(patients, mapping = aes(x = Height)) +
  geom_histogram()

select(patients, Height)

str_remove(patients$Height, "cm")
as.numeric(str_remove(patients$Height, "cm"))

patients <- mutate(patients, Height = as.numeric(str_remove(Height, "cm")))
patients

ggplot(patients, mapping = aes(x = Height)) +
  geom_histogram()

# You can modify multiple columns within a single mutate function call.

patients <- read.delim("patient-data.txt")
patients <- as_tibble(patients)
patients <- mutate(patients,
                   Sex = as.factor(str_trim(Sex)),
                   Height = as.numeric(str_remove(Height, "cm")))
patients

```
