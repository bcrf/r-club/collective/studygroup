# Data Manipulation and Visualisation using R and Tidyverse

Attendance: 5

## In class - github

We discussed the github **"fork"** button that allows one to make a personal copy of a github repository.


## In class - RStudio

We followed some material from the online Intermediate R Course [Data Manipulation and Visualisation using R](http://bioinformatics-core-shared-training.github.io/r-intermediate/).

We followed section titled [Tidying and transforming data](https://rawgit.com/bioinformatics-core-shared-training/r-intermediate/master/2.dplyr-intro-live-coding-script.html)

The [complete material is on github](https://github.com/bioinformatics-core-shared-training/r-intermediate) including the data files used during the exercises. The easiest is to download the complete repository:

- View the Project on GitHub: [bioinformatics-core-shared-training/r-intermediate](https://github.com/bioinformatics-core-shared-training/r-intermediate)
- At top right click on green button "Code" and download as a Zip file `r-intermediate-master.zip`.
- unZip the file and all files are contained within the `r-intermediate-master` directory.

Then:  
- Create a new RStudio project.
- Open file `2.dplyr-intro-solutions.Rmd` to get the code.

## Data files explored

We only used data files `clinical-data.txt` and `patient-data.txt` all contained within the `r-intermediate-master` directory.

The commands all work as they did in 2019 with one exception: there was a major change when R went from version 3.x to version 4.x. I wrote a Blog about this: [Strings as factors in R](https://bcrf.biochem.wisc.edu/2020/11/20/strings-as-factors-in-r/) that explains how the default `stringsAsFactors = F` reads data simply as "strings" and do not assign a factor status by default.

Hence the one difference was to add `as.factor` to the command: `levels(as.factor(patients$Sex))`

The added command `levels(as.factor(str_trim(patients$Sex)))` was used after the white spaces were trimmed.

All the rest is the same, with the added "cosmetic" exception that Tidyverse does not report the contents of table in exacly the same way.
