## Tidyverse for genomics

Attendance: 6

Tidyverse is a new way to work with R that is more conducive to data analysis. The most famous tidyverse package is `ggplot2`.

We learned commands from tidyverse packages `tidyr` and `dyplr`. See keypoints below.

To prepare working with more genomics data with R we explored an introductory Tidyverse commands based on this tutorial:

[Introduction to R for Biologists](https://melbournebioinformatics.github.io/r-intro-biologists/intro_r_biologists.html)
(Short URL: [go.wisc.edu/5j6iif](https://go.wisc.edu/5j6iif).)

## Key Points

- Tabular data can be loaded into R with the tidyverse functions `read_csv()` and `read_tsv()`
- Tidyverse functions such as `pivot_longer()`, `mutate()`, `filter()`, `select()`, `full_join()` can be used to manipulate data
- A ggplot has 3 components: data (dataset), mapping (columns to plot) and geom (type of plot). Different types of plots include `geom_point()`, `geom_jitter()`, `geom_line()`, `geom_boxplot()`, `geom_violin()`.
- `facet_wrap()` can be used to make subplots of the data
- The aesthetics of a ggplot can be modified, such as colouring by different columns in the dataset, adding labels or changing the background

## Materials

The complete source code is available on [github](https://github.com/melbournebioinformatics/r-intro-biologists)

I converted the main page [intro_r_biologists.Rmd](https://github.com/melbournebioinformatics/r-intro-biologists/blob/master/intro_r_biologists.Rmd) into slides, that can be found in the `slides` directory.

## Docker

We explored briefly what **Docker** can do and all users created a username at [docker.com](https://www.docker.com/). We tried a container that runs Rstudio within a browser. There was a problem that I have now identified as an incompatibility for the M1 chip architecture `arm64`. The container was a  `amd64` version.


## Plans for next session

Next time we'll explore the [bioconductor container](https://hub.docker.com/r/bioconductor/bioconductor_docker/) and Tidy-transcriptomics:

-  [tidy-transcriptomics-manifesto](https://stemangiola.github.io/tidytranscriptomics/post/tidy-transcriptomics-manifesto/)  - [Archived](http://web.archive.org/web/20230125114451/https://stemangiola.github.io/tidytranscriptomics/post/tidy-transcriptomics-manifesto/)
- [tidytranscriptomics](https://stemangiola.github.io/ABACBS2020_tidytranscriptomics/articles/tidytranscriptomics.html)
