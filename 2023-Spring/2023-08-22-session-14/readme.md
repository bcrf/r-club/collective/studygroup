# R/Rstudio Genomics - continued

We'll continue the R/RStudio for genomics that we started in the previous session (See [Session 13](https://git.doit.wisc.edu/bcrf/r-club/collective/studygroup/-/tree/main/2023-Spring/2023-08-08-session-13) for details and links.)

We'll follow the material on the "["all in one page"](https://datacarpentry.org/genomics-r-intro/aio.html) link, starting with the short section titled [**Importing data from Excel**](https://datacarpentry.org/genomics-r-intro/aio.html#importing-data-from-excel).

Files needed:

- `Ecoli_metadata.xlsx`
- `combined_tidy_vcf.csv`

which can be downloaded from local copy in Session 13 [`data`](https://git.doit.wisc.edu/bcrf/r-club/collective/studygroup/-/tree/main/2023-Spring/2023-08-08-session-13/data) directory.

Files can also be downloaded from Terminal with `wget` or `curl` (see Session 13), here is the code for `curl` which is installed by default on Mac and Windows.

```
curl -o Ecoli_metadata.xlsx https://git.doit.wisc.edu/bcrf/r-club/collective/studygroup/-/raw/main/2023-Spring/2023-08-08-session-13/data/Ecoli_metadata.xlsx 

curl -o combined_tidy_vcf.csv https://git.doit.wisc.edu/bcrf/r-club/collective/studygroup/-/raw/main/2023-Spring/2023-08-08-session-13/data/combined_tidy_vcf.csv

```

## Rstudio project

We'll continue using the same RStudio "project" created in the last session.

If necessary this link [create an Rstudio project](https://datacarpentry.org/genomics-r-intro/aio.html#create-an-rstudio-project) will provide details.

