## Molecular Data Analysis Using R

We will use **some** data or scripts from  ***"Molecular Data Analysis Using R"*** (2017, Csaba Ortutay and Zsuzsanna Ortutay.)

> ***The book is available online*** [^1] *via* the UW Library [^2]. 

The scripts and data files are available at:

- https://www.wiley.com/go/ortutay/molecular_data_analysis_r
- also on github: https://github.com/csortu/MDAuR

Citation: [^3]   
Ortutay, C & Ortutay, Z 2017, *Molecular Data Analysis Using R*. New Jersey. https://doi.org/10.1002/9781119165057

---


[^1]: The library then refers to this link at: [ebookcentral.proquest.com](https://ebookcentral.proquest.com/lib/wisc/detail.action?pq-origsite=primo&docID=4774521). Howewver, it may be necessary to start the search at library.wisc.edu .

[^2]: UW Library catalog number: https://search.library.wisc.edu/catalog/9912822968302121/online

[^3]: more formats. https://researchportal.tuni.fi/en/publications/molecular-data-analysis-using-r

