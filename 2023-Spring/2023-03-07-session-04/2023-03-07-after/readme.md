# 2023-03-07-after

This directory contains the files created during the session.
See `convert-to-ggplot-part-1.Rmd` and HTML rendering.

We also explore a graphical interface to create ggplot: package `esquisse` ([info vignette](https://cran.r-project.org/web/packages/esquisse/vignettes/get-started.html).)
