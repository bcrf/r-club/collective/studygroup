## 2023-03-07

We'll start looking at the book "Molecular Data Analysis Using R" (2017, Csaba Ortutay and Zsuzsanna Ortutay.) See links within [previoius communication](https://git.doit.wisc.edu/bcrf/r-club/collective/studygroup/-/blob/main/2023-Spring/_Communications/2023-02-21-new-approach.md). (Code at [github.com/csortu/MDAuR](https://github.com/csortu/MDAuR).)

The R language code within the book and its related scripts is in the orginal stype and format of R, nowadays referred to as ***base R***. For this session, we'll explore some base R code in the book used for plotting, and learn how to redo the plot with the more modern `ggplot2` package.

This is also an opportunity to explore documents and report makings in the form of
the explanatory `*.Rmd` files (processed derivatives: `pdf`, `html`, `docx`.)

- Part I explores the first plot (files `baseR2ggplot.*`)
- Part II explores the second, more complex plot (files `baseR2ggplot-part2.*`.)

Part I was treated as a more fancy report, with citations, table of contents, table of figures and table of tables, as a way to show what can be accomplished.

The included `2023-03-07-before.zip` file contains all files necessary for running the code:

```
├── baseR2ggplot-part2.Rmd
├── baseR2ggplot-part2.docx
├── baseR2ggplot-part2.html
├── baseR2ggplot-part2.pdf
├── baseR2ggplot.Rmd
├── baseR2ggplot.docx
├── baseR2ggplot.html
├── baseR2ggplot.pdf
├── data
│   └── EGFPpH7_em.csv
│       ├── EGFPpH7_em.csv
│       ├── EGFPpH7_exc.csv
│       ├── R-PE-801ph75_em.csv
│       └── R-PE-801ph75_exc.csv
├── ref.bib
└── scripts
    ├── facs_analysis.R
    └── plotspectra.R
```

More files may be added after the actual session.
