# R-Club - session 15 - 2023-09-20

***Note***: THis is the LAST session for the Spring-2023 club, which in fact covered Spring+Summer.

Addendance: 6

We continued the topic started in session 13

- R GENOMICS: https://datacarpentry.org/genomics-r-intro/
- ALL IN ONE PAGE INSTRUCTOR VIEW: https://datacarpentry.org/genomics-r-intro/instructor/aio.html

We started at paragraph titled **Data frame bonus material: math, sorting, renaming**

Since it's a new session it is necessary to read-in the data again:

```
variants <- read.csv("combined_tidy_vcf.csv")
```
Then we can ask the question of what is tha lowest (`min`) or highest (`max`) value within the `DP` column which represents "Read Depth".


```
max(variants$DP)
min(variants$DP)
```

Sorting data based on a specific column, here with `DP`.

```
sorted_by_DP <- variants[order(variants$DP), ]
```

## Question

We explored the difference between the base-R command `read.csv()` and the tidyverse command `read_csv()`.

The Tidyverse object is said to be 2x as fast to use by R.

Both create object that have "data frame" status, but the Tidyverse object is also a "Tibble". One main difference is when data is output on the console: the Tidyverse object prints only columns within the console space area.

## Sorting

Using the base-R object:

```
sorted_by_DP <- variants[order(variants$DP), ]
head(sorted_by_DP)
```

Using the Tidyverse object:

```
sorted_by_DP_2d <- variants_2[order(variants_2$DP, decreasing=TRUE), ]
head(sorted_by_DP_2d)
```

Chris then pointed out that it is possible to sort by decreasing order by simply adding a `-` sign to the column name to be sorted, here `-variants_2$DP`. In that case `decreasing=TRUE` is not necessary.

```
sorted_test <- variants_2[order(-variants_2$DP), ]
head(sorted_test)
```

## Column name editing

The provided code replaces `sample_id` by `strain`.

```
colnames(variants)[colnames(variants) == "sample_id"] <- "strain"
colnames(variants)
```

It can be noted that in this case it is not necessary to know the order of the columns, just its name.

Using the column order (in appearance within the header) the code may be shorter, but does require to know the column number. Here `sample_id` appears within column `1` in which case the same result can be achieved with:

```
# create a copy
V <- variants
colnames(V)

# Change name of column 1
colnames(V)[1] <- "NEW_NAME"
colnames(V)
```

## Saving data into a file

We compared the resulting CSV file from saving the base-R object `variants` with either the base-R `write.csv()` or the Tidyverse `write_csv()`.

```
write.csv(variants, file = "SRR2584863_variants.csv")

write_csv(variants, file = "SRR2584863_variants_2.csv")
```


The main difference is that the Tidyverse saving does not include the row names, and the header of the CSV file does not contain as many double quotes.

### Discussion

There are differences between USA and European saving formats, hence the existence of read and write `csv2` functions.

- Decimal point: ` . ` USA but ` , ` Europe
- Separator: ` , ` USA (comma separated), but ` ; ` Europe: semi-colon separator.

Other options exist as well: `delim` provides a space delimitor while `sep='\t'`provides a TAB separator.

## Import XL file

The RStudio menu **File > Import Dataset...** provides the code to conduct the import:

```
# READ XL file
Ecoli_metadata <- read_excel("Ecoli_metadata.xlsx")
View(Ecoli_metadata)
```
