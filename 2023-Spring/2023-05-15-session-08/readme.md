## In class

We explored the following resource below.

Source material in "restructured text" (a "cousin" of markdown) format and PDF is available at 
https://github.com/avrilcoghlan/LittleBookofRBioinformatics

The contents is still relevant even though it has not been updated in a long time.

### Little Book of R for Bioinformatics

Open:
* https://a-little-book-of-r-for-bioinformatics.readthedocs.io/en/latest/

Scroll down to find the "Chapters in this Book"

We decided to explore "Comparative Genomics" which is [Chapter 9](https://a-little-book-of-r-for-bioinformatics.readthedocs.io/en/latest/src/chapter9.html) 

The chapter uses Bioconductor package [biomaRt](https://bioconductor.org/packages/release/bioc/html/biomaRt.html)

If necessary install with:

```{r}
if (!require("BiocManager", quietly = TRUE))
    install.packages("BiocManager")

BiocManager::install("biomaRt")
```
BiomaRt access databases, mostly ENSEMBL and the names have evolved since the write-up of this chapter. However, we found that all commands are still relevant. We used the chicken (*gallus* in Latin) instead of the protist examples in the chapter.

Here are the commands that I used in the context of the chapter. See the chapter text for reference and explanations.

```{r}
ensemblSession8 <- useMart("ENSEMBL_MART_ENSEMBL")

listDatasets(ensemblSession8)

ensemblgallus <- useDataset("ggallus_gene_ensembl", mart=ensemblSession8)

class(ensemblgallus)

gallusattributes <- listAttributes(ensemblgallus)

class(gallusattributes)

attributenames <- gallusattributes[[1]]

attributedescriptions <- gallusattributes[[2]]

length(attributenames)

attributenames[1:10]

attributedescriptions[1:10]

?getBM
```

```{r}
gallusgenes <- getBM(attributes = c("ensembl_gene_id"), mart=ensemblgallus)

gallusgenes <- getBM(attributes = c("ensembl_gene_id", "description"), mart=ensemblgallus)

gallusnames <- gallusgenes[[1]]

length(gallusnames)

gallusnames[1:5]

gallusgenes2 <- getBM(attributes = c("ensembl_gene_id", "gene_biotype"), mart=ensemblgallus)
```

```{r}
gallusnames2 <- gallusgenes2[[1]]

gallusgenebiotypes2 <- gallusgenes2[[2]]

table(gallusgenebiotypes2)

```


```{r}
gallusgenes <- getBM(attributes = c("ensembl_gene_id","hsapiens_homolog_ensembl_gene","hsapiens_homolog_orthology_type"), mart = ensemblgallus)

gallusgenenames <- gallusgenes[[1]]

gallusorthologues <- gallusgenes[[2]]

gallusorthologuetypes  <- gallusgenes[[3]]

# check out contents
gallusgenenames[1:5]
gallusorthologues[1:5]
gallusorthologues[10:15]
gallusorthologuetypes[1:5]
gallusorthologuetypes[20:30]
gallusorthologuetypes[50:60]
```

Use `factor()` to find how many options are listed:

```{r}
factor(gallusorthologuetypes)
```
Answer: 3 levels:

```
Levels:  ortholog_many2many ortholog_one2many ortholog_one2one
```




