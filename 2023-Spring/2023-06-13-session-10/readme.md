# Session 10
June 13, 2023

Attendance: 3

Note: This page can be accessed with the shortcut: https://go.wisc.edu/2qrm15

## Proposed subject: Survival Analysis

In class we followed this tutorial:

- Survival Analysis Basics - Easy Guides: http://www.sthda.com/english/wiki/survival-analysis-basics

The commands used are saved in file `in_class.R`.

Note: Using a web browser "reader" setting hides the commercial panels.

### Other existing online tutorials:


- Survival Analysis with R (2017-09-25) - https://rviews.rstudio.com/2017/09/25/survival-analysis-with-r/
- (***Tidyverse***) Survival Analysis in R (2022-10-19) - Emily C. Zabor https://www.emilyzabor.com/tutorials/survival_analysis_in_r_tutorial.html
- Stats and R - What is survival analysis? Examples by hand and in R Antoine Soetewey 2022-12-22 - https://statsandr.com/blog/what-is-survival-analysis/

### Github project

https://github.com/PMacDaSci/survival-analysis-intro

(We'll use to see how to create HTML from project.)

In class we copied this project and use the `knitr` button in RStudio to compile the `SurvivalAnalysis_I.Rmd` file into an HTML output.

*note*: we had to run the package installation within the file by hand as there was a complaint about installing without a CRAN mirror.

### Some videos

- Survival Analysis Part 5 | Kaplan Meier Model in R with RStudio -  https://youtu.be/6_AF9mMuk9E
- (part of a series found on channel [@marinstatlectures](https://www.youtube.com/@marinstatlectures/videos))

-  Mar 31, 2023 Workshop (2h48min) Introduction to Survival Analysis in R - UCLA Office of Advanced Research Computing - https://youtu.be/Y_83HXuHMdc
