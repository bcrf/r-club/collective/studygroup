# Session 12

Tidyverse functions to replace `apply()` family.

See "help" file *e.g.* https://rdrr.io/r/base/lapply.html

Google Search: *what command to use to replace lapply in Tidyverse*


Links:

- https://stackoverflow.com/questions/45101045/why-use-purrrmap-instead-of-lapply

- https://community.rstudio.com/t/which-tidyverse-is-the-equivalent-to-search-replace-from-spreadsheets/3548

- https://stackoverflow.com/questions/62428677/replacing-string-in-multiple-dataframes-with-lapply


FROM:  R for Data Science - Hadley Wickham -  https://r4ds.had.co.nz

- https://r4ds.had.co.nz/iteration.html


### Purr Tidyverse package

[purrr *v.s.* apply](https://jtr13.github.io/spring19/ss5593&fq2150.html) Shengjie Sun(ss5593), Feng QIU(fq2150) March 23, 2019 (Archived: [Aug 13, 2022](https://web.archive.org/web/20220813035804/https://jtr13.github.io/spring19/ss5593&fq2150.html))


### Dyplyr

```{r}
library(dplyr)
starwars
```

### Package Repurrrsive

```{r}
install.packages("repurrrsive")
```
Contains:

    - sw_people
    - sw_films
    - sw_planets
    - sw_species
    - sw_vehicles
    - sw_starships

Now we can run the [purrr *v.s.* apply](https://jtr13.github.io/spring19/ss5593&fq2150.html)  examples.

```{r}
map_chr(sw_films,~.x$title)
```
Make a table from the `sw_films` list (from https://sparkbyexamples.com/r-programming/convert-list-to-r-dataframe/)
Even though there is an error, the table will be created....
BUT: elements (columns) will still be considered a LIST.

```{r}
# Convert nested list to the dataframe by columns
x <-as.data.frame(do.call(cbind,sw_films))

# Convert nested list to the dataframe by rows
y <-as.data.frame(do.call(rbind,sw_films))
```

### Lambda anonymous functions

This is cryptic, but the function can be defined as `"[["` to "grab" the double brackets from the list.
See [purrr *v.s.* apply](https://jtr13.github.io/spring19/ss5593&fq2150.html)  examples.

```{r}
apply(sw_films, "[[", "director")
lapply(sw_films, "[[", "director")[1]
lapply(sw_films, "[[", "title")
```


The specifically defined function examples are:

```{r}
# purrr::map() way
purrr::map(sw_films, ~.x[["director"]])
# lapply
lapply(sw_films, function(x){x[["director"]]})

```

Weird output... keeps the `[[` brackets
```
library(plyr)

# Titles
plyr::llply(sw_films, "[[", 1)

# Summaries of movies
plyr::llply(sw_films, "[[", 3)
```

FROM: https://www.projectpro.io/recipes/what-are-anonymous-functions-R

>What are anonymous functions in R? Anonymous functions are those functions which are not assigned a variable name. These functions are also known as lambda functions (just like the ones in python) These functions are just created for the time being and are used without defining a variable name to them.

