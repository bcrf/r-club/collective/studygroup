# First Fall session

Attendance: 5

## Materials

Inspired by one of the materials listed in the [2023-10-09 email](https://git.doit.wisc.edu/bcrf/r-club/collective/studygroup/-/blob/main/2023-Fall/Emails/2023-10-09.md) we started to explore the freely available online book:

[**R for Data Science** (2e)](https://r4ds.hadley.nz/) since the first author, Hadley Wickham, is the principal author of the modernization of R with the "Tidyverse" series packages.

To catch up:

- read the [Intro](https://r4ds.hadley.nz/intro) section
- follow [Chapter 2](https://r4ds.hadley.nz/data-visualize)  "Data Visualize" and its use of the modern plotting with `ggplot2`.

The file [2023-10-10-in-class.R](https://git.doit.wisc.edu/bcrf/r-club/collective/studygroup/-/blob/main/2023-Fall/2023-10-10-session-01/2023-10-10-in-class.R) contains the code used in class, which is based on the book code with some added variations.

## Next session

The Club meets every other week, so the next session will be October 24
