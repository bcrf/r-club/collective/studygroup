# 2023-10-10
# Exploring Data Science 2e
# https://r4ds.hadley.nz/intro

################################################
# Intro plot
################################################

# Load ggplot2 package
library(ggplot2)
# mpg is a dataset contained within the ggplot package

ggplot(mpg, aes(displ, hwy)) +
  geom_point(aes(color = class))

ggplot(mpg, aes(displ, hwy)) +
  geom_point(aes(color =  manufacturer))

# Create an R object containing mpg data
mpg_data <- mpg

colnames(mpg)

ggplot(mpg, aes(displ, hwy)) +
  geom_point(aes(color =  year))

ggplot(mpg, aes(displ, hwy)) +
  geom_point(aes(color =  as.factor(year)))

ggplot(mpg, aes(displ, hwy)) +
  geom_point(aes(color =  as.character(year)))

ggplot(mpg, aes(displ, hwy)) +
  geom_point(aes(color =  model))

ggplot(mpg, aes(displ, hwy)) +
  geom_point(aes(color =  fl))

# Install tidyverse
install.packages("tidyverse")
# INSTALL MOre PACKAGES

install.packages(
  c("arrow", "babynames", "curl", "duckdb", "gapminder", 
    "ggrepel", "ggridges", "ggthemes", "hexbin", "janitor", "Lahman", 
    "leaflet", "maps", "nycflights13", "openxlsx", "palmerpenguins", 
    "repurrrsive", "tidymodels", "writexl"))

# Activate some:
library(tidyverse)
library(palmerpenguins)
library(ggthemes)

# data()fuction copies penguins data as object
# see on the "Environment" panel in RStudio
data(penguins)

glimpse(penguins)

################################################
# Chapter 2.2
################################################
# Recreate plot

ggplot(data = penguins)

ggplot(
  data = penguins,
  mapping = aes(x = flipper_length_mm, y = body_mass_g)
)

ggplot(
  data = penguins,
  mapping = aes(x = flipper_length_mm, y = body_mass_g)
) +
  geom_point()


ggplot(
  data = penguins,
  mapping = aes(x = flipper_length_mm, y = body_mass_g, color = species)
) +
  geom_point()



ggplot(
  data = penguins,
  mapping = aes(x = flipper_length_mm, y = body_mass_g, color = species)
) +
  geom_point() +
  geom_smooth(method = "lm")


ggplot(
  data = penguins,
  mapping = aes(x = flipper_length_mm, y = body_mass_g)
) +
  geom_point(mapping = aes(color = species)) +
  geom_smooth(method = "lm")

#
ggplot(
  data = penguins,
  mapping = aes(x = flipper_length_mm, y = body_mass_g)
) +
  geom_point(mapping = aes(color = species)) +
  geom_smooth(method = "lm")


ggplot(
  data = penguins,
  mapping = aes(x = flipper_length_mm, y = body_mass_g)
) +
  geom_point(mapping = aes(color = species, shape = species)) +
  geom_smooth(method = "lm")

#
ggplot(
  data = penguins,
  mapping = aes(x = flipper_length_mm, y = body_mass_g)
) +
  geom_point(aes(color = species, shape = species)) +
  geom_smooth(method = "lm") +
  labs(
    title = "Body mass and flipper length",
    subtitle = "Dimensions for Adelie, Chinstrap, and Gentoo Penguins",
    x = "Flipper length (mm)", y = "Body mass (g)",
    color = "Species", shape = "Species"
  ) +
  scale_color_colorblind()


# SEPARATE Color and Shape for Legend:

ggplot(
  data = penguins,
  mapping = aes(x = flipper_length_mm, y = body_mass_g)
) +
  geom_point(aes(color = species, shape = species)) +
  geom_smooth(method = "lm") +
  labs(
    title = "Body mass and flipper length",
    subtitle = "Dimensions for Adelie, Chinstrap, and Gentoo Penguins",
    x = "Flipper length (mm)", y = "Body mass (g)",
    color = "Species", shape = "Shape"
  ) +
  # scale_color_colorblind()
  scale_color_excel_new()

################################################
#  2.2.5 Exercises
# https://r4ds.hadley.nz/data-visualize#exercises
################################################

# 1. How many rows are in penguins? How many columns?
  
dim(penguins)
# [1] 344   8
length(penguins)
# [1] 8

# What does the bill_depth_mm variable in the penguins data frame describe? Read the help for ?penguins to find out.

# >>> a number denoting bill depth (millimeters)

#### Make a scatterplot of bill_depth_mm vs. bill_length_mm. That is, make a scatterplot with bill_depth_mm on the y-axis and bill_length_mm on the x-axis. Describe the relationship between these two variables.

ggplot(
  data = penguins,
  mapping = aes(x = bill_length_mm, y = bill_depth_mm)
) + geom_point(aes(color = species, shape = species)) 


# What happens if you make a scatterplot of species vs. bill_depth_mm? What might be a better choice of geom?

ggplot(
  data = penguins,
  mapping = aes(x = species, y = bill_depth_mm)
) + geom_point(aes(color = species, shape = species)) 

ggplot(
  data = penguins,
  mapping = aes(x = species, y = bill_depth_mm)
) + 
  geom_boxplot(aes(color = species, shape = species)) 

ggplot(
  data = penguins,
  mapping = aes(x = species, y = bill_depth_mm)
) + 
  geom_boxplot(aes(color = species)) 

ggplot(
  data = penguins,
  mapping = aes(x = species, y = bill_depth_mm, color = species)
) + 
  geom_boxplot() 

# 5. Why does the following give an error and how would you fix it?

ggplot(data = penguins) + 
  geom_point()

# There is no aes() => ggplot does not know what to plot.

# 6. What does the na.rm argument do in geom_point()? What is the default value of the argument? Create a scatterplot where you successfully use this argument set to TRUE.

ggplot(
  data = penguins,  
  mapping = aes(x = bill_length_mm, y = bill_depth_mm)
) + geom_point(na.rm = TRUE, aes(color = species, shape = species)) 

# Note: having na.rm = TRUE removes warning "Removed 2 rows containing missing values..."

# 7. Add the following caption to the plot you made in the previous exercise: “Data come from the palmerpenguins package.” Hint: Take a look at the documentation for labs().

ggplot(
  data = penguins,  
  mapping = aes(x = bill_length_mm, y = bill_depth_mm)
) + geom_point(na.rm = TRUE, aes(color = species, shape = species))  +
  labs(
    title = "...see caption",
    caption = "Data come from the palmerpenguins package.")

# 8. Recreate the following visualization. What aesthetic should bill_depth_mm be mapped to? And should it be mapped at the global level or at the geom level?

ggplot(
  data = penguins,  
  mapping = aes(x = flipper_length_mm, y = body_mass_g)
  )+
    geom_point(aes(color = bill_depth_mm)) +
  geom_smooth()
 
# 9. Run this code in your head and predict what the output will look like. Then, run the code in R and check your predictions.

ggplot(
  data = penguins,
  mapping = aes(x = flipper_length_mm, y = body_mass_g, color = island)
) +
  geom_point() +
  geom_smooth(se = FALSE)

# 10. Will these two graphs look different? Why/why not?

par(mfrow = c(1, 2))

p1 <- ggplot(
  data = penguins,
  mapping = aes(x = flipper_length_mm, y = body_mass_g)
) +
  geom_point() +
  geom_smooth()


p2 <- ggplot() +
  geom_point(
    data = penguins,
    mapping = aes(x = flipper_length_mm, y = body_mass_g)
  ) +
  geom_smooth(
    data = penguins,
    mapping = aes(x = flipper_length_mm, y = body_mass_g)
  )

par(mfrow = c(1, 1))

# Suggestion by Chris to plot side by side with cowplot rather than par(mfrow)

library(cowplot)

plot_grid(p1,p2)


