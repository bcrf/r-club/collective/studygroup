# Fourth Fall session

Attendance: 5

## Materials

- We continued [Chapter 3](https://r4ds.hadley.nz/data-transform) on **"Data transformation"**.
- We followed sections:
-  - [3.2.3 arrange()](https://r4ds.hadley.nz/data-transform#arrange): changes the order of the rows based on the value of the columns.
- - [3.2.4 distinct()](https://r4ds.hadley.nz/data-transform#distinct): finds all the unique rows in a dataset.

Then a good amount of time was spent on [3.2.5 Exercises](https://r4ds.hadley.nz/data-transform#exercises)

- we extensively made use of the new pipe `|>` 

a chapter that we'll take at a slow pace due to the importance of data transformation using the TidyVerse `dplyr` package. This chapter also uses a new dataset: "flights that departed from New York City in 2013."

In this session:
-  we followed the section about the `filter()` function
- made use of the new pipe `|>` which is now generalized within R, superseeding mostly the `%>%` pipe requiring the `maggritr` pacakge.

To catch-up, go over the sections mentioned above, inclusing [3.2.5 Exercises](https://r4ds.hadley.nz/data-transform#exercises)

The file [2023-11-21-in-class.R](https://git.doit.wisc.edu/bcrf/r-club/collective/studygroup/-/raw/main/2023-Fall/2023-11-21-session-04/2023-11-21-in-class.R) contains the code used in class, including exercises done.
