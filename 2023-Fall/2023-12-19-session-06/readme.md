# Sixth Fall session

Attendance: 2 

## Materials

> Note: I added 2 blog entries directly related to this session at these links:   
> [Data exploration in R with Tidyverse dplyr – part 1 – overview](https://bcrf.biochem.wisc.edu/2023/12/26/data-exploration-in-r-with-tidyverse-dplyr-part-1-overview/)   
> [Data exploration in R with Tidyverse dplyr – part 2 – 10 examples](https://bcrf.biochem.wisc.edu/2023/12/26/data-exploration-in-r-with-tidyverse-dplyr-part-2-10-examples/)

<HR>

We improvised a lesson on ***markdown*** and ***Rmarkdown*** which are extremly useful for document creation in RStudio.

For details see my series of posts on this subject:

1. [Do yourself a favor: learn Markdown -- Master it in 10 minutes!](https://bcrf.biochem.wisc.edu/2021/06/03/do-yourself-a-favor-learn-markdown-master-it-in-10-minutes/).  
2. [Do yourself a favor: learn Markdown -- Episode 2](https://bcrf.biochem.wisc.edu/2021/06/04/do-yourself-a-favor-learn-markdown-episode-2/).  
3. [Do yourself a favor: learn Markdown -- Episode 3, Academic writing](https://bcrf.biochem.wisc.edu/2021/06/10/do-yourself-a-favor-learn-markdown-episode-3/).  
4. [Do yourself a favor: learn Markdown -- Episode 4. Reproducible reports](https://bcrf.biochem.wisc.edu/2021/06/11/do-yourself-a-favor-learn-markdown-episode-4-reproducible-reports/).  
5. [Do yourself a favor: learn Markdown -- Episode 5. BibTeX interface for PubMed](https://bcrf.biochem.wisc.edu/2022/10/28/do-yourself-a-favor-learn-markdown-episode-5-bibtex-interface-for-pubmed/).    
6. [Learn Markdown -- Episode 6. Convert Word to Markdown with pandoc](https://bcrf.biochem.wisc.edu/2022/12/30/learn-markdown-episode-6-convert-word-to-markdown-with-pandoc/).

The "Definitive" resource is the freely available online book: [**R Markdown: The Definitive Guide**](https://bookdown.org/yihui/rmarkdown/)

## In CLASS

In class we reviewed **markdown** and some **Rmarkdown** extensions. 
We used what appeared to be a simple blog depicting a few usages of `dplyr` the package we are in the process of learning in Chapter 3 the book we are following [R for Data Science 2nd edition](https://r4ds.hadley.nz/).

Therefore we proceeded to write an Rmarkdown document to implement these examples after the translation to English.

All the details are inside the `.Rmd` original document that can then be "knitted into" HTML, PDF and MSWord with the `knit` button from RStution which in turn calls on the `knitr` package.

One of the goal was to also update the code from the `magrittr` pipe `%>%` to the new R-base pipe `|>`.

Starting with section 2.7, some issues (resolved) occurred with this conversion. See text within.

## The Pipe

During the convertion after class, 
I encountered major difference between the pipes. This rather technical article 
was very useful to understand the differences: ["Understanding the native R pipe |>"](https://towardsdatascience.com/understanding-the-native-r-pipe-98dea6d8b61b), [saved archive](http://web.archive.org/web/20230000000000*/https://towardsdatascience.com/understanding-the-native-r-pipe-98dea6d8b61b).

# Credits

The origninal script are from: (see in class document for translation)
https://www.maximaformacion.es/blog-ciencia-datos/top-10-manipulacion-de-datos-con-dplyr/
