# 2023 Fall sessions

This directory can be accessed directly by  UW shortcut [go.wisc.edu/xd6s20](https://go.wisc.edu/xd6s20)

Each session summary or material will be contained within a directory named with the ISO method of YYYY-MM-DD which provides that the sorting is automatically ordered by date.

The Emails directory will contain (some) of the emails sent to the group list.
