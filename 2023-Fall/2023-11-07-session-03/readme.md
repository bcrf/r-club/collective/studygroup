# Third Fall session

Attendance: 3

## Materials

- We finsihed [Chapter 2](https://r4ds.hadley.nz/data-visualize) on data visualization from [**R for Data Science** (2e)](https://r4ds.hadley.nz/) and tested various options of the `ggsave()` function to save plots in various graphical formats, `png` and `pdf` in particular.

- We started [Chapter 3](https://r4ds.hadley.nz/data-transform) titled **"Data transformation"**, a chapter that we'll take at a slow pace due to the importance of data transformation using the TidyVerse `dplyr` package. This chapter also uses a new dataset: "flights that departed from New York City in 2013."

In this session:
-  we followed the section about the `filter()` function
- made use of the new pipe `|>` which is now generalized within R, superseeding mostly the `%>%` pipe requiring the `maggritr` pacakge.

To catch-up, follow sections `3.2.1 filter()` to (and including) `3.2.2 Common mistakes`.

The file [2023-11-07-in-class.R](https://git.doit.wisc.edu/bcrf/r-club/collective/studygroup/-/blob/main/2023-Fall/2023-11-07-session-03/2023-11-07-in-class.R) contains the code used in class, which is based on the book code with some added variations.

## Next session

The Club meets every other week, so the next session will be November 21.

We'll continue with [Chapter 3](https://r4ds.hadley.nz/data-transform) at section: `3.2.3 arrange()`.

