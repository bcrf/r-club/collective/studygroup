# Second Fall session

Attendance: 7

## Materials

We continued [Chapter 2](https://r4ds.hadley.nz/data-visualize) on data visualization from [**R for Data Science** (2e)](https://r4ds.hadley.nz/).

To catch-up, follow sections  from `2.3 ggplot2 calls` to (and including) ` 2.5.5 Exercises`.

The file [2023-10-24-in-class.R](https://git.doit.wisc.edu/bcrf/r-club/collective/studygroup/-/blob/main/2023-Fall/2023-10-24-session-02/2023-10-24-in-class.R) contains the code used in class, which is based on the book code with some added variations.

## Next session

The Club meets every other week, so the next session will be November 07.

