# 2023-10-24
# Exploring Data Science 2e
# https://r4ds.hadley.nz/intro

############################################### 3 ggplot2 calls
# Continued
################################################
library(tidyverse)
library(palmerpenguins)

ggplot(penguins, aes(x = species)) +
  geom_bar()

ggplot(penguins, aes(x = fct_infreq(species))) +
  geom_bar()

ggplot(penguins, aes(x = body_mass_g)) +
  geom_histogram(binwidth = 200)

ggplot(penguins, aes(x = body_mass_g)) +
  geom_histogram()

ggplot(penguins, aes(x = body_mass_g)) +
  geom_histogram(binwidth = 100)

ggplot(penguins, aes(x = body_mass_g)) +
  geom_histogram(binwidth = 500)

ggplot(penguins, aes(x = body_mass_g)) +
  geom_histogram(binwidth = 20)
ggplot(penguins, aes(x = body_mass_g)) +
  geom_histogram(binwidth = 2000)

# DENSITY

ggplot(penguins, aes(x = body_mass_g)) +
  geom_density()


#  2.4.3 Exercises
# 1. Make a bar plot of species of penguins, where you assign species to the y aesthetic. How is this plot different?

ggplot(penguins, aes(y = species)) +
  geom_bar()

#> COOL - makes it sideways!

#2. How are the following two plots different? 

ggplot(penguins, aes(x = species)) +
geom_bar(color = "red")

ggplot(penguins, aes(x = species)) +
  geom_bar(fill = "red")

# 3. What does the bins argument in geom_histogram() do?
# = change the size of bars

# 4. Make a histogram of the carat variable in the diamonds dataset that is available when you load the tidyverse package. Experiment with different binwidths. What binwidth reveals the most interesting patterns?

ggplot(diamonds, aes(x = carat)) +
  geom_histogram(binwidth = 0.05)

ggplot(diamonds, aes(x = carat)) +
  geom_histogram(binwidth = 0.01)

#  2.5 Visualizing relationships

ggplot(penguins, aes(x = species, y = body_mass_g)) +
  geom_boxplot()

ggplot(penguins, aes(x = body_mass_g, color = species)) +
  geom_density(linewidth = 0.75)

ggplot(penguins, aes(x = body_mass_g, color = species)) +
  geom_density(linewidth = 2.0)

ggplot(penguins, aes(x = body_mass_g, color = species)) +
  geom_density(linewidth = 0.5)

# Default is indeed 0.5
ggplot(penguins, aes(x = body_mass_g, color = species)) +
  geom_density()

## Add fill color:
ggplot(penguins, aes(x = body_mass_g, color = species, fill = species)) +
  geom_density(alpha = 0.5)

# No alpha means we can see through
ggplot(penguins, aes(x = body_mass_g, color = species, fill = species)) +
  geom_density()

ggplot(penguins, aes(x = body_mass_g, color = species, fill = species)) +
  geom_density(alpha = 0.3)

#  2.5.2 Two categorical variables
ggplot(penguins, aes(x = island)) +
  geom_bar()

ggplot(penguins, aes(x = island, fill = species)) +
  geom_bar()

# % 
ggplot(penguins, aes(x = island, fill = species)) +
  geom_bar(position = "fill")

# 2.5.3 Two numerical variables

# Scatter plot:

ggplot(penguins, aes(x = flipper_length_mm, y = body_mass_g)) +
  geom_point()

ggplot(penguins, aes(x = flipper_length_mm, y = body_mass_g, color=species)) +
  geom_point()

#  2.5.4 Three or more variables

ggplot(penguins, aes(x = flipper_length_mm, y = body_mass_g)) +
  geom_point(aes(color = species, shape = island))

# Facetting

ggplot(penguins, aes(x = flipper_length_mm, y = body_mass_g)) +
  geom_point(aes(color = species, shape = species)) +
  facet_wrap(~island)

ggplot(penguins, aes(x = flipper_length_mm, y = body_mass_g)) +
  geom_point(aes(color = species, shape = island)) +
  facet_wrap(~species)

# Using ~ as formula
ggplot(penguins, aes(x = flipper_length_mm, y = body_mass_g)) +
  geom_point(aes(color = species, shape = island)) +
  facet_wrap(island ~ species)

#  2.5.5 Exercises

# The mpg data frame that is bundled with the ggplot2 package contains 234 observations collected by the US Environmental Protection Agency on 38 car models. Which variables in mpg are categorical? Which variables are numerical? (Hint: Type ?mpg to read the documentation for the dataset.) How can you see this information when you run mpg?

# 2 - Make a scatterplot of hwy vs. displ using the mpg data frame. Next, map a third, numerical variable to color, then size, then both color and size, then shape. How do these aesthetics behave differently for categorical vs. numerical variables?

ggplot(mpg, aes(x= hwy, y=displ)) +
  geom_point()

ggplot(mpg, aes(x= hwy, y=displ, color=cyl)) +
  geom_point()

ggplot(mpg, aes(x= hwy, y=displ, color=as.character(cyl))) +
  geom_point()

ggplot(mpg, aes(x= hwy, y=displ, color=as.factor(cyl))) +
  geom_point(aes(shape=trans))

ggplot(mpg, aes(x= hwy, y=displ)) +
  geom_point(aes(shape=trans, color=as.factor(cyl)))

# 3. In the scatterplot of hwy vs. displ, what happens if you map a third variable to linewidth?

ggplot(mpg, aes(x= hwy, y=displ)) +
  geom_point(aes(linewidth=3))

ggplot(mpg, aes(x= hwy, y=displ, linewidth=3)) +
  geom_point()

# > Nothing happens...

#  4 . What happens if you map the same variable to multiple aesthetics?

ggplot(mpg, aes(x= hwy, y=displ)) +
  geom_point(aes(shape=as.factor(cyl), color=as.factor(cyl)))

# 5. Make a scatterplot of bill_depth_mm vs. bill_length_mm and color the points by species. What does adding coloring by species reveal about the relationship between these two variables? What about faceting by species?

ggplot(penguins, aes(x=bill_depth_mm, y = bill_length_mm, color=species)) +
  geom_point()

ggplot(penguins, aes(x=bill_depth_mm, y = bill_length_mm, color=species)) +
  geom_point() +
  facet_wrap(~species)

ggplot(penguins, aes(x=bill_depth_mm, y = bill_length_mm, color=species)) +
  geom_point() +
  facet_grid(~species)

# Why does the following yield two separate legends? How would you fix it to combine the two legends?

ggplot(
  data = penguins,
  mapping = aes(
    x = bill_length_mm, y = bill_depth_mm, 
    color = species, shape = species
  )
  
# 
) +
  geom_point() +
  labs(color = "Species", shape = "Species")

#

ggplot(penguins, aes(x = island, fill = species)) +
  geom_bar(position = "fill")
ggplot(penguins, aes(x = species, fill = island)) +
  geom_bar(position = "fill")
