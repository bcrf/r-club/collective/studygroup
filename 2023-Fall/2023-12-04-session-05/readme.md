# Fifth Fall session

Attendance: 3 (2 excused)

## Materials

- We continued [Chapter 3](https://r4ds.hadley.nz/data-transform) on **"Data transformation"**.
- We followed sections:
- - [3.3 Columns](https://r4ds.hadley.nz/data-transform#columns)
- - [3.4 The pipe](https://r4ds.hadley.nz/data-transform#sec-the-pipe)
- - [3.5 Groups](https://r4ds.hadley.nz/data-transform#groups)

## Columns
Quote from [3.3 Columns](https://r4ds.hadley.nz/data-transform#columns)   
>There are four important verbs that affect the columns without changing the rows:   
- `mutate()` creates new columns that are derived from the existing columns, 
- `select()` changes which columns are present, 
- `rename()` changes the names of the columns, and 
- `relocate()` changes the positions of the columns.

## pipe

Details on the new R-base pipe `|>` that can replace the `maggrittr` package pipe `%>%`.

## Groups

>...most important functions: `group_by()`, `summarize()`, and the slice family of functions.


### In Class file

The file [2023-12-05-in-class.R](https://git.doit.wisc.edu/bcrf/r-club/collective/studygroup/-/raw/main/2023-Fall/2023-12-04-session-05/2023-12-05-in-class.R) contains the code used in class, including exercises done.


### Catch-up

To catch-up, go over the sections mentioned above, up to and including [The `slice_` functions](https://r4ds.hadley.nz/data-transform#the-slice_-functions)

### Next session
Next session will start with: [3.5.4 Grouping by multiple variables](https://r4ds.hadley.nz/data-transform#grouping-by-multiple-variables)

