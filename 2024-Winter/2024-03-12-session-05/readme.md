# Session 05

Attendance: 4

## Session 

Conor presented his attempt to download bacterial genomes within R using the [`biomart`](https://bioconductor.org/packages/release/bioc/html/biomaRt.html) pacakge from [Biocondoctor](https://bioconductor.org).

In class we did not find an answer using R directly but discovered the possibility to download genome files from NCBI using their [**command line tools**](https://www.ncbi.nlm.nih.gov/datasets/docs/v2/download-and-install/), in this case the `datasets` tool.

However, after class it was found that by default the function `getGenome()` does not look at Bacterial data:

```
Due to its extended dataset size (GenBank: >700 MB, RefSeq: >150 MB) Kingdom 'bacteria' 
will not be downloaded by default anymore. To also include 'bacteria' please specify 
the argument 'skip_bacteria = FALSE'
```

Therefore, adding `skip_bacteria = FALSE` for one of the genomes resulted in the download (rather than `[1] "Not available"`)

```
getGenome(db = "genbank", organism = 1506 , reference = FALSE, skip_bacteria = FALSE)
```

```
-> Starting genome retrieval of '1506' from genbank ...

|--------------------------------------------------|
|==================================================|

More than one entry has been found for '1506'. Only the first entry 'Clostridium sp.' has been used for subsequent genome retrieval. If you wish to download a different version, please use the NCBI accession ID when specifying the 'organism' argument. See ?is.genome.available for examples.
-> GENOME download of 1506 is completed!
-> Checking md5 hash of file: _ncbi_downloads/genomes/1506_genomic_genbank.fna.gz (md5: ef20f40c5c55564fb6b5426122a161d0) ...
-> The md5 hash of file '_ncbi_downloads/genomes/1506_md5checksums.txt' matches!
-> The GENOME has been downloaded to '_ncbi_downloads/genomes' and has been named '1506_genomic_genbank.fna.gz'.

Please cite: Drost HG, Paszkowski J. Biomartr: genomic data retrieval with R. Bioinformatics (2017) 33(8): 1216-1217. doi:10.1093/bioinformatics/btw821.
[1] "_ncbi_downloads/genomes/1506_genomic_genbank.fna.gz"
```

There is information about retrieving alternate genome sequences within the text above.

MISSION ACCOMPLISHED!
