# 1rst 2024 session

Attendance: 4

# Materials

We are learning about GitHub from RStudio for further use later.

Repository created in class: https://github.com/jsgro/LearnGit

Notes:

- Windows users: had to install `git`
- Mac users: had to go through the Token installation, see YouTube: [ How To Fix Support For Password Authentication Was Removed On GitHub ](https://youtu.be/ePCBuIQJAUc)
