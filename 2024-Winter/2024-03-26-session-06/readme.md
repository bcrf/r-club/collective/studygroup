# Session 06

Attendance: 3

## Materials

In session 4 Meghan had presented materials from [github.com/tjgibson/S2_pioneers_manuscript](https://github.com/tjgibson/S2_pioneers_manuscript) in the [`workflow/scripts`](https://github.com/tjgibson/S2_pioneers_manuscript/tree/main/workflow/scripts) that is related to creating figures for the publication of Gibson et al. (ref below). 

In class we were interested to find how to create figures as in the paper figure 1 panel `e` or figure 2 panel `c` that are graphical represenations of the "center" of binding in Chip-Seq data.

Chris took the lead to start a script (not based on the github scripts) using a `bed` file as well as big-wig files provided by Meghan. Files are large and possibly unpublished and therefore not included in this repository.

The script file (`R_club_bigwig_heatmap_3.26.24.R`) is included here for reference, but it not finished. We may finish it at the next session.



## References
Gibson, T.J., Larson, E.D., and Harrison, M.M.. (2024) Protein-intrinsic properties and context-dependent effects regulate pioneer factor binding and function. *Nat Struct Mol Biol*. [doi:10.1038/s41594-024-1231-8](https://doi.org/10.1038/s41594-024-01231-8) PubMed PMID: [38365978](https://pubmed.ncbi.nlm.nih.gov/38365978/). 
