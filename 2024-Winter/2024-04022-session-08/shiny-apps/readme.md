# Materials

In class we decided for: learn about Shiny, a GUI way to running R code. I found the lesson on the SSCC humanities web site: https://sscc.wisc.edu/sscc/pubs/shiny/index.html - We can try to see if we can run the examples.

## Examples done in class.

This directory contains some examples done in class or collected online elsewhere.

- Example app `app-example01.R` is from https://sscc.wisc.edu/sscc/pubs/shiny/index.html

- modified as `app-example-01-2.R`

- Starwars app `starwars-app-01.R` is from https://stackoverflow.com/questions/64686860/dplyr-pivot-table-rshiny


## Changing variable name: `app2.R`

Thanks to Yury for working out changing the name of the passed variable `input$var` to a nicer name.

The implemented solution is found in file `app2.R` which as the added steps:

line 4: `var_labels <- c("wt" = "Weight", "mpg" = "MPG", "hp" = "Horsepower")`

This line inverts the `=` assigment compared to the `choices` line further below.

The name of the variables is then passed within lines 17 and 18

line 17: `var_label <- var_labels[input$var]`
line 18: `paste("The average of", var_label, "is", mean(mtcars[, input$var]))`

On line 18 `var_label` (singular) is assigned a value taken from the `var_labels` (plural) list and extracted using a square bracket `[]` nomenclature. 
For example, if the chosen variable is `wt` then thanks to the `var+labels` assigments in line 4 we can match `wt` with `Weights`. This is then printed within line 18.
