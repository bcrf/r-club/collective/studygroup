# Session 08

Attendance: 4

## Materials

See details in subdirectory `shiny-apps` since we decided to go with this option (see "Possible topics" below.)

We briefly looked at publishing a shiny app with a Docker container.
We may explore this further during the next session using *e.g.* the [`rocker/shiny`](https://hub.docker.com/r/rocker/shiny) container 

(See https://rocker-project.org/images/versioned/shiny.html)


<hr>

## Possible topics

**(from email sent yesterday)**

we could do one of the following (or anything else!)

Explore the ChIPseeker package from the vignette https://bioconductor.org/packages/release/bioc/vignettes/ChIPseeker/inst/doc/ChIPseeker.html as it offers functions to create the kind of heatmap we were creating from scratch in the previous sessions.

FYI I found a new ChIP-seq tutorial that is interesting at:
https://hbctraining.github.io/Intro-to-ChIPseq-flipped/
(github:https://github.com/hbctraining/Intro-to-ChIPseq-flipped)

learn about Shiny, a GUI way to running R code. I found the lesson on the SSCC humanities web site: https://sscc.wisc.edu/sscc/pubs/shiny/index.html - We can try to see if we can run the examples.

Of course, these are just suggestions... and we can address any other topic that is of interest to you..
