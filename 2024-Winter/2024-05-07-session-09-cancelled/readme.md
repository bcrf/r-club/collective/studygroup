# Session 09

*Cancelled*

Multiple users could not make it so this session was cancelled.

## Planned Materials

Planned for next session we could get inspired by the following resources:

Free Online *O'Reilly* **Book**:  "[Mastering Shiny]https://mastering-shiny.org/index.html" 

- Github : [hadley/mastering-shiny](https://github.com/hadley/mastering-shiny)
- Author: [Hadley Wickham](https://hadley.nz/) (author of `ggplot`​)

Other examples/tutorials that I gleaned over time that could be used at some point:

- [intro-to-shiny-hands-on-workshop](https://github.com/aniaskrzydlo/intro-to-shiny-hands-on-workshop)
- [R-Shinyapp-Tutorial](https://github.com/aagarw30/R-Shinyapp-Tutorial)
- [patrickbarks/shiny-bayes](https://github.com/patrickbarks/shiny-bayes)
