# Session 3 - 2024-01-30

Today we digressed to test the [**`ggmagnify`**](https://github.com/hughjonesd/ggmagnify) package.
This pacakge provides supplemental commands to create an "inset" within a `ggplot2` illustration.

There were a few issues raised, which are answered within the R code file for today class:

### In Class file

The file [2024-30-01-in-class.R](https://git.doit.wisc.edu/bcrf/r-club/collective/studygroup/-/raw/main/2024-Winter/2024-01-30-session-03/2024-01-30-in-class.R) contains the code used in class.


## Earlier after class notes

***Notes***

In class we had  participants with both Macs and Windows systems

Examples with rectangle (square-ish) inset shapes appear to work fine and resemble the examples provided by the authors.

However, when testing alternate shapes we started to note discrepencies in our plots *vs* those on the package [githup page](https://github.com/hughjonesd/ggmagnify).

The main issue is that on insets with shapes other than rectanglar formats the background is not removed and even some plotted points are carried over, therefore adding "fake points" to the plot.

The easiest visual to clarify this discrepency is the example using the 
state of Texas, pulled as inset. On our Mac/Windows system the area around the map, in blue, is not removed.

We investigated with a Docker version of RStudio that runs under Linux. Some images could not be easily reproduced as the container required libraries not installed on the OS (and requiring root password to instsall.) However, we could clearly see a difference in *e.g.* the hexadecimal shape inset example.

