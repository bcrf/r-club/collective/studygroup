# 2024-16-05
# Exploring Data Science 2e
# https://r4ds.hadley.nz/intro

library(tidyverse)
library(nycflights13)

############################################### 
#   3.5.4 Grouping by multiple variables
############################################### 

daily <- flights |>  
  group_by(year, month, day)
daily
tail(daily)

daily_flights <- daily |> 
  summarize(n = n())

# `summarise()` has grouped output by 'year', 'month'. You can
# override using the `.groups` argument.

# explicitly request to suppress the message:
daily_flights <- daily |> 
  summarize(
    n = n(), 
    .groups = "drop_last"
  )

?summarize

# .groups	
# [Experimental] Grouping structure of the result.

# "drop_last": dropping the last level of grouping. This was the only supported option before version 1.0.0.

# "drop": All levels of grouping are dropped.

# "keep": Same grouping structure as .data.

# "rowwise": Each row is its own group.

##################
# 3.5.5 Ungrouping
##################
# remove grouping from a data frame without using summarize()  with ungroup().

dim(daily)
# [1] 336776     19

daily |> 
  ungroup()

# what happens when you summarize an ungrouped data frame:

daily |> 
  ungroup() |>
  summarize(
    avg_delay = mean(dep_delay, na.rm = TRUE), 
    flights = n()
  )

# You get a single row back because dplyr treats all the rows in an ungrouped data frame as belonging to one group.

##################
# 3.6 .by
##################

#dplyr 1.1.0 includes a new, experimental, syntax for per-operation grouping, the .by argument. group_by() and ungroup() aren’t going away, but you can now also use the .by argument to group within a single operation:
  
# .by works with all verbs and has the advantage that you don’t need to use the .groups argument to suppress the grouping message or ungroup() when you’re done.

flights |> 
  summarize(
    delay = mean(dep_delay, na.rm = TRUE), 
    n = n(),
    .by = month
  )

# Or if you want to group by multiple variables:
  
  flights |> 
  summarize(
    delay = mean(dep_delay, na.rm = TRUE), 
    n = n(),
    .by = c(origin, dest)
  )

#################  
# 3.5.7 Exercises
#################  

# 1. Which carrier has the worst average delays? Challenge: can you disentangle the effects of bad airports vs. bad carriers? Why/why not? 
  # (Hint: think about :
  # flights |> group_by(carrier, dest) |> summarize(n())
  
  flights |> group_by(carrier, dest) |> summarize(n())
  
  flights |> 
    summarize( delay = mean(dep_delay, na.rm = TRUE), 
               .by = c(carrier, dest),
               n())
  
  flights |> 
    summarize( delay = mean(dep_delay, na.rm = TRUE), 
               .by = c(dest, carrier),
               n()) |> arrange(carrier, dest)

# Delay depends on destination:  
flights |> group_by(carrier, dest) |> 
  summarize(delay = mean(dep_delay, na.rm = TRUE), n())
  
#
  flights |> group_by(carrier) |> summarize(delay = mean(dep_delay, na.rm = TRUE)) |> arrange(desc(delay))

# 2. Find the flights that are most delayed upon departure from each destination.

flights |> summarize( .by = c(carrier, dep_delay),
                      n()) |> arrange(dep_delay)
  
flights |> group_by(dest) |> arrange(desc(dep_delay)) |> slice_head(n=1)

# 3. How do delays vary over the course of the day. Illustrate your answer with a plot.

del <- flights |> group_by(dep_time, dep_delay) |> summarize(n())

ggplot(del) +
  aes(x = dep_time, y = dep_delay) +
  geom_jitter(size = 0.5) +
  geom_smooth() +
  theme_minimal()


