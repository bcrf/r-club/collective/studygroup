# 2024-01-16 - 2nd Winter session

Attendance: 7

## Materials

- We continued [Chapter 3](https://r4ds.hadley.nz/data-transform) on **"Data transformation"**.
- We finished section [3.5 Groups](https://r4ds.hadley.nz/data-transform#groups)
- - starting where we left off with [3.5.4 Grouping by multiple variables](https://r4ds.hadley.nz/data-transform#grouping-by-multiple-variables)

### In Class file

The file [2024-16-05-in-class.R](https://git.doit.wisc.edu/bcrf/r-club/collective/studygroup/-/raw/main/2024-Winter/2024-01-16-session-02/2024-16-05-in-class.R) contains the code used in class.








