# Session 04

Attendance: 3

## Materials

Meghan presented an R Script related to create a PDF of a genomic section based on a BigWig file.
Script is derived from [github.com/tjgibson/S2_pioneers_manuscript](https://github.com/tjgibson/S2_pioneers_manuscript) in the [`workflow/scripts`](https://github.com/tjgibson/S2_pioneers_manuscript/tree/main/workflow/scripts) section, particularly figures 1, 2, or 3. The repository was created for the publication:

Gibson, T.J., Larson, E.D., and Harrison, M.M.. (2024) Protein-intrinsic properties and context-dependent effects regulate pioneer factor binding and function. *Nat Struct Mol Biol*. [doi:10.1038/s41594-024-1231-8](https://doi.org/10.1038/s41594-024-01231-8) PubMed PMID: [38365978](https://pubmed.ncbi.nlm.nih.gov/38365978/). 

We explored both the creation of the image, which are "peaks" from ChipSeq data, as well as how this can be added as various items within a PDF file.

The "in class" file can be found as The file [2024-02-27-in-class.R](https://git.doit.wisc.edu/bcrf/r-club/collective/studygroup/-/raw/main/2024-Winter/2024-02-27-session-04/2024-02-27-in-class.R) contains the code used in class.

The script was kept simple, to print one track segment, which can be found in file [`browser_track.pdf`](https://git.doit.wisc.edu/bcrf/r-club/collective/studygroup/-/blob/main/2024-Winter/2024-02-27-session-04/browser_track.pdf).

Note: Figure 1 of the Gibson paper shows multiple tracks on a single figure: https://www.nature.com/articles/s41594-024-01231-8/figures/1

The BigWig files are not included in this repository due to their size.

The BigWig files need to match the genome they represent. In the short script this is accomplished by loading the following: `library(TxDb.Dmelanogaster.UCSC.dm6.ensGene)` for Drosophila.

BIGWIG format: https://academic.oup.com/bioinformatics/article/30/11/1618/282756
