# Combee R Study Group: Functions

The Computational Biology, Ecology, & Evolution - [ComBEE](https://combee-uw-madison.github.io/studyGroup/)  - is a group of researchers at UW-Madison interested in computational biology in ecology and evolution.

This folder contains the discussion we had about Functions.

Example source tutorials: 

- R Functions at [w3schools.com](https://www.w3schools.com/r/r_functions.asp)
- How to Write Functions in R (with 18 Code Examples) at [dataquest.io](https://www.dataquest.io/blog/write-functions-in-r/)
- Chapter 19: Functions in [R for Data Science](https://r4ds.had.co.nz/functions.html)

## Example function done in class

During the session we created a function that takes an input a number `x` given by the user, and then plots `x` random numbers on a plot, but also provides the user with the actual numbers. See comments in the `R` script file [FunctionExample.R](https://git.doit.wisc.edu/bcrf/r-club/collective/studygroup/-/raw/main/2023-contrib/2023-11-15-Combee-R-functions/FunctionExample.R)
